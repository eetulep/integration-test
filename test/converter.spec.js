// TDD - unit-testing

const expect = require("chai").expect;
const converter = require("../src/converter");

describe("Color Code Converter", () => {
  describe("RGB to Hex conversions", () =>{
    it("Converts the basic colors", () => {
      const redHex = converter.rgbToHex(255, 0, 0);
      expect(redHex).to.equal("#ff0000");

      const blueHex = converter.rgbToHex(0, 0, 255);
      expect(blueHex).to.equal("#0000ff");
    });
  });
  describe("Hex to RGB conversions", () => {
    it("Converts the basic colors", () => {
      const redRgb = converter.hexToRgb("#ff0000");
      expect(redRgb).to.deep.equal([255, 0, 0]);

      const blueRgb = converter.hexToRgb("#0000ff");
      expect(blueRgb).to.deep.equal([0, 0, 255]);

      const greenRgb = converter.hexToRgb("#00ff00");
      expect(greenRgb).to.deep.equal([0, 255, 0]);
    });
  });
});